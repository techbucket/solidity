Solidity Smart Contracts
=======================

Smart contract examples

These contracts can be ran on etherium platform.

Just install the metamask, deploy it to test network


Requirements
============

* metamask
* https://remix.ethereum.org

Compiling & Deployment
============

    open remix ide and select solidity
    create a new file
    compile the code
    deploy the code
    copy the contract address after deployment

Mastering solidity
============

* solidity https://solidity.readthedocs.io/en/v0.5.11/

Credits
=======

* Arul Armstrong